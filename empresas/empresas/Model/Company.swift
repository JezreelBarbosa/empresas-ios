//
//  Company.swift
//  empresas
//
//  Created by Jezreel Barbosa on 29/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Foundation

class Company: Codable {
    // Static Properties
    
    static var list: [Company] = []
    
    // Static Methods
    // Public Types
    
    struct EnterpriseType: Codable, Mappable {
        let id: Int
        let enterprise_type_name: String
        
        init(mapper: Mapper) {
            self.id = mapper.keyPath("id")
            self.enterprise_type_name = mapper.keyPath("enterprise_type_name")
        }
    }
    
    // Public Properties
    
    let id: Int
    let email_enterprise: String?
    let facebook: String?
    let twitter: String?
    let linkedin: String?
    let phone: String?
    let own_enterprise: Bool
    let enterprise_name: String?
    let photo: String?
    let description: String?
    let city: String?
    let country: String?
    let value: Int?
    let share_price: Double?
    let enterprise_type: EnterpriseType
    
    // Public Methods
    // Initialisation/Lifecycle Methods
    // Override Methods
    // Private Types
    // Private Properties
    // Private Methods
}
