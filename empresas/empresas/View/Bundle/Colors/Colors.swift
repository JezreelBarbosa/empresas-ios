//
//  Colors.swift
//  empresas
//
//  Created by Jezreel Barbosa on 27/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

extension UIColor {
    /// Background Color #ebe9d7
    static var appBackgroundColor: UIColor {
        return UIColor(named: "appBackgroundColor")!
    }
    
    /// Medium Pink #ee4c77
    static var appMediumPink: UIColor {
        return UIColor(named: "appMediumPink")!
    }
    
    /// Greeny Blue #57bbbc
    static var appGreenyBlue: UIColor {
        return UIColor(named: "appGreenyBlue")!
    }
    
    /// Charcoal Gray #383743
    static var appCharcoalGray: UIColor {
        return UIColor(named: "appCharcoalGray")!
    }
    
    /// Warning Red #ff0f44
    static var appWarningRed: UIColor {
        return UIColor(named: "appWarningRed")!
    }
    
    /// Inactive Gray #748383
    static var appInactiveGray: UIColor {
        return UIColor(named: "appInactiveGray")!
    }
    
    /// Title Blue
    static var appTitleBlue: UIColor {
        return UIColor(named: "appTitleBlue")!
    }
    
    /// Warm Gray
    static var appWarmGray: UIColor {
        return UIColor(named: "appWarmGray")!
    }
    
    /// Red Placeholder
    static var appRedPlaceholder: UIColor {
        return UIColor(named: "appRedPlaceholder")!
    }
}
