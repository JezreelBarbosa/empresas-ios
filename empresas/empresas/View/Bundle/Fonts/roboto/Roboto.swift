//
//  Roboto.swift
//  empresas
//
//  Created by Jezreel Barbosa on 27/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

extension UIFont {
    enum RobotoFontWeight: String {
        case medium = "Roboto-Medium"
        case bold = "Roboto-Bold"
        case regular = "Roboto-Regular"
        case italic = "Roboto-Italic"
    }
    
    static func roboto(_ weight: RobotoFontWeight, size: CGFloat) -> UIFont? {
        return UIFont(name: weight.rawValue, size: size)
    }
}
