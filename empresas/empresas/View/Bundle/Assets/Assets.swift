//
//  Assets.swift
//  empresas
//
//  Created by Jezreel Barbosa on 27/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

extension UIImage {
    /// Home Logo
    static var appHomeLogo: UIImage {
        return UIImage(named: "logo_home")!
    }
    
    /// Email Icon
    static var appEmailIcon: UIImage {
        return UIImage(named: "emailIcon")!
    }
    
    /// Lock Icon
    static var appLockIcon: UIImage {
        return UIImage(named: "lockIcon")!
    }
    
    /// Navigation Logo
    static var appNavigationLogo: UIImage {
        return UIImage(named: "navigationLogo")!
    }
}
