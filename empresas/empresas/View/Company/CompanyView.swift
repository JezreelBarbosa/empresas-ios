//
//  CompanyView.swift
//  empresas
//
//  Created by Jezreel Barbosa on 28/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class CompanyView: UIView {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    
    let tableView = UITableView()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    // Public Methods
    
    func customizeNavigationItem(_ viewController: UIViewController) {
        guard let navController = viewController.navigationController else { return }
        
        let image = UIImage.appNavigationLogo
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height

        let bannerX = bannerWidth / 2 - (image.size.width) / 2
        let bannerY = bannerHeight / 2 - (image.size.height) / 2

        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit
        viewController.navigationItem.titleView = imageView
        viewController.navigationItem.title = "Empresas"
    }
    
    func customizeSearchBar() {
        if isSearchBarCustomized { return }
        isSearchBarCustomized = true
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.searchController.obscuresBackgroundDuringPresentation = false
            self.searchController.searchBar.tintColor = .white
            self.searchController.searchBar.searchTextField.backgroundColor = .clear
            self.searchController.searchBar.searchTextField.leftView?.tintColor = .white
            self.searchController.searchBar.searchTextField.font = UIFont.roboto(.regular, size: 17)
            self.searchController.searchBar.searchTextField.textColor = .white
            self.searchController.searchBar.searchTextField.borderStyle = .none
            
            let line = UIView()
            let search = self.searchController.searchBar.searchTextField
            search.addSubview(line)
            
            line.translatesAutoresizingMaskIntoConstraints = false
            line.leadingAnchor.constraint(equalTo: search.leadingAnchor).isActive = true
            line.trailingAnchor.constraint(equalTo: search.trailingAnchor).isActive = true
            line.bottomAnchor.constraint(equalTo: search.bottomAnchor).isActive = true
            line.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            line.backgroundColor = .white
            
            self.searchController.searchBar.searchTextField.clearButtonMode = .never
            self.searchController.searchBar.searchTextField.attributedPlaceholder = NSAttributedString(string: "Pesquisar",attributes: [
                .font: UIFont.roboto(.regular, size: 17)!,
                .foregroundColor: UIColor.appRedPlaceholder
            ])
        }
    }
    
    // Initialisation/Lifecycle Methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        renderSuperView()
        renderLayout()
        renderStyle()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        renderSuperView()
        renderLayout()
        renderStyle()
    }
    
    // Override Methods
    // Private Types
    // Private Properties
    
    private var isSearchBarCustomized: Bool = false
    
    // Private Methods
    
    private func renderSuperView() {
        addSubview(tableView)
    }
    
    private func renderLayout() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        
        layoutIfNeeded()
    }
    
    private func renderStyle() {
        backgroundColor = .appBackgroundColor
        
        tableView.backgroundColor = .appBackgroundColor
        tableView.separatorStyle = .none
    }
}
