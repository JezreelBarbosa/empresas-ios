//
//  CompanyDetailsView.swift
//  empresas
//
//  Created by Jezreel Barbosa on 29/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class CompanyDetailsView: UIView {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    // Public Methods
    
    func fillCompanyDetails(_ viewController: UIViewController, company: CompanyViewModel?) {
        self.viewController = viewController
        let titleAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, .font: UIFont.roboto(.regular, size: 17)!]
        viewController.navigationController?.navigationBar.titleTextAttributes = titleAttributes
        
        viewController.navigationItem.title = company?.name
        companyTextLabel.text = company?.description
    }
    
    func clearInformation() {
        viewController?.navigationItem.title = nil
        companyTextLabel.text = nil
    }
    
    // Initialisation/Lifecycle Methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        renderSuperView()
        renderLayout()
        renderStyle()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        renderSuperView()
        renderLayout()
        renderStyle()
    }
    
    // Override Methods
    // Private Types
    // Private Properties
    
    private weak var viewController: UIViewController?
    
    private let companyImageView = UIImageView()
    private let companyTextLabel = UILabel()
    
    // Private Methods
    
    private func renderSuperView() {
        addSubview(companyImageView)
        addSubview(companyTextLabel)
    }
    
    private func renderLayout() {
        subviews.forEach({$0.translatesAutoresizingMaskIntoConstraints = false})
        
        companyImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        companyImageView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        companyImageView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        companyImageView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        
        companyTextLabel.topAnchor.constraint(equalTo: companyImageView.bottomAnchor, constant: 16).isActive = true
        companyTextLabel.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        companyTextLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        companyTextLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        
        layoutIfNeeded()
    }
    
    private func renderStyle() {
        backgroundColor = .appBackgroundColor
        
        companyImageView.backgroundColor = .green
        
        companyTextLabel.font = UIFont.roboto(.regular, size: 17)
        companyTextLabel.textAlignment = .left
        companyTextLabel.textColor = .appWarmGray
        companyTextLabel.numberOfLines = 0
    }
}
