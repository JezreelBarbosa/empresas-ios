//
//  CompanyTableViewCell.swift
//  empresas
//
//  Created by Jezreel Barbosa on 28/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {
    // Static properties
    
    static let reuseIdentifier: String = "CompanyTableViewCell"
    static let rowHeight: CGFloat = 116.0
    
    // Public Types
    // Public Properties
    // Public Methods
    
    func fill(company: CompanyViewModel) {
        companyNameLabel.text = company.name
        companyTypeLabel.text = company.typeName
        companyLocationLabel.text = company.country
    }
    
    // Initialisation/Lifecycle Methods

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        renderSuperView()
        renderLayout()
        renderStyle()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        renderSuperView()
        renderLayout()
        renderStyle()
    }
    
    // Override Methods
    
    override func prepareForReuse() {
        companyNameLabel.text = nil
        companyTypeLabel.text = nil
        companyLocationLabel.text = nil
    }
    
    // Private Types
    // Private Properties
    
    private let companyContentView = UIView()
    
    private let companyImageView = UIImageView()
    private let companyNameLabel = UILabel()
    private let companyTypeLabel = UILabel()
    private let companyLocationLabel = UILabel()
    
    // Private Methods
    
    private func renderSuperView() {
        addSubview(companyContentView)
        companyContentView.addSubview(companyImageView)
        companyContentView.addSubview(companyNameLabel)
        companyContentView.addSubview(companyTypeLabel)
        companyContentView.addSubview(companyLocationLabel)
    }
    
    private func renderLayout() {
        companyContentView.translatesAutoresizingMaskIntoConstraints = false
        companyContentView.subviews.forEach({$0.translatesAutoresizingMaskIntoConstraints = false})
        
        companyContentView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        companyContentView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        companyContentView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        companyContentView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        companyImageView.topAnchor.constraint(equalTo: companyContentView.topAnchor, constant: 10).isActive = true
        companyImageView.leadingAnchor.constraint(equalTo: companyContentView.leadingAnchor, constant: 10).isActive = true
        companyImageView.bottomAnchor.constraint(equalTo: companyContentView.bottomAnchor, constant: -10).isActive = true
        companyImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        companyNameLabel.topAnchor.constraint(equalTo: companyContentView.topAnchor, constant: 10).isActive = true
        companyNameLabel.leadingAnchor.constraint(equalTo: companyImageView.trailingAnchor, constant: 16).isActive = true
        companyNameLabel.trailingAnchor.constraint(equalTo: companyContentView.trailingAnchor, constant: -16).isActive = true
        
        companyTypeLabel.topAnchor.constraint(equalTo: companyNameLabel.bottomAnchor, constant: 4).isActive = true
        companyTypeLabel.leadingAnchor.constraint(equalTo: companyImageView.trailingAnchor, constant: 16).isActive = true
        companyTypeLabel.trailingAnchor.constraint(equalTo: companyContentView.trailingAnchor, constant: -16).isActive = true
        
        companyLocationLabel.topAnchor.constraint(equalTo: companyTypeLabel.bottomAnchor, constant: 4).isActive = true
        companyLocationLabel.leadingAnchor.constraint(equalTo: companyImageView.trailingAnchor, constant: 16).isActive = true
        companyLocationLabel.trailingAnchor.constraint(equalTo: companyContentView.trailingAnchor, constant: -16).isActive = true
        
        layoutIfNeeded()
    }
    
    private func renderStyle() {
        selectionStyle = .none
        backgroundColor = .clear
        
        companyContentView.backgroundColor = .white
        
        companyImageView.backgroundColor = .green
        
        companyNameLabel.font = UIFont.roboto(.bold, size: 17)
        companyNameLabel.text = "Company Name"
        companyNameLabel.textAlignment = .left
        companyNameLabel.textColor = .appTitleBlue
        companyNameLabel.numberOfLines = 1
        
        companyTypeLabel.font = UIFont.roboto(.italic, size: 17)
        companyTypeLabel.text = "Business"
        companyTypeLabel.textAlignment = .left
        companyTypeLabel.textColor = .appWarmGray
        companyTypeLabel.numberOfLines = 1
        
        companyLocationLabel.font = UIFont.roboto(.regular, size: 17)
        companyLocationLabel.text = "Location"
        companyLocationLabel.textAlignment = .left
        companyLocationLabel.textColor = .appWarmGray
        companyLocationLabel.numberOfLines = 1
    }
}
