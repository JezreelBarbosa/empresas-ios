//
//  LoginView.swift
//  empresas
//
//  Created by Jezreel Barbosa on 27/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class LoginView: UIView {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    
    let emailTextField = UITextField()
    let passwordTextField = UITextField()
    
    let enterButton = UIButton(type: .custom)
    
    // Public Methods
    
    func setWarningInformationHidden(_ hidden: Bool) {
        constraints.first(where: {$0.identifier == "emailConstraint"})?.constant = hidden ? 12 : -8
        emailLineView.backgroundColor = hidden ? .appCharcoalGray : .appWarningRed
        emailWarningView.isHidden = hidden
        
        constraints.first(where: {$0.identifier == "passwordConstraint"})?.constant = hidden ? 12 : -8
        passwordLineView.backgroundColor = hidden ? .appCharcoalGray : .appWarningRed
        passwordWarningView.isHidden = hidden
        
        errorMessageLabel.isHidden = hidden
    }
    
    func setEnterButtonActive(_ active: Bool) {
        enterButton.isUserInteractionEnabled = active
        enterButton.backgroundColor = active ? .appGreenyBlue : .appInactiveGray
    }
    
    func setEnterButtonLoadingState(_ state: Bool) {
        enterButton.setAttributedTitle(NSAttributedString(string: state ? "" : "ENTRAR",attributes: [
            .font: UIFont.roboto(.bold, size: 16)!,
            .foregroundColor: UIColor.white
        ]), for: .normal)
        enterButtonActivityIndicatorView.isHidden = !state
    }
    
    // Initialisation/Lifecycle Methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        renderSuperView()
        renderLayout()
        renderStyle()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        renderSuperView()
        renderLayout()
        renderStyle()
    }
    
    // Override Methods
    // Private Types
    // Private Properties
    
    private let logoImageView = UIImageView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    
    private let emailIconImageView = UIImageView(image: .appEmailIcon)
    private let emailWarningView = UIView()
    private let emailWarningLabel = UILabel()
    private let emailLineView = UIView()
    
    private let passwordIconImageView = UIImageView(image: .appLockIcon)
    private let passwordWarningView = UIView()
    private let passwordWarningLabel = UILabel()
    private let passwordLineView = UIView()
    
    private let errorMessageLabel = UILabel()
    
    private let enterButtonActivityIndicatorView = UIActivityIndicatorView()
    
    // Private Methods
    
    private func renderSuperView() {
        addSubview(logoImageView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        
        addSubview(emailTextField)
        addSubview(emailLineView)
        addSubview(emailIconImageView)
        addSubview(emailWarningView)
        emailWarningView.addSubview(emailWarningLabel)
        
        addSubview(passwordTextField)
        addSubview(passwordLineView)
        addSubview(passwordIconImageView)
        addSubview(passwordWarningView)
        passwordWarningView.addSubview(passwordWarningLabel)
        
        addSubview(errorMessageLabel)
        addSubview(enterButton)
        addSubview(enterButtonActivityIndicatorView)
    }
    

    private func renderLayout() {
        emailWarningLabel.translatesAutoresizingMaskIntoConstraints = false
        passwordWarningLabel.translatesAutoresizingMaskIntoConstraints = false
        subviews.forEach({$0.translatesAutoresizingMaskIntoConstraints = false})
        
        logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 56).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 185).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        titleLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 44).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -44).isActive = true
        titleLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 48).isActive = true
        
        subtitleLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 44).isActive = true
        subtitleLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -44).isActive = true
        subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16).isActive = true
        
        
        emailIconImageView.centerYAnchor.constraint(equalTo: emailTextField.centerYAnchor).isActive = true
        emailTextField.centerYAnchor.constraint(equalTo: emailWarningView.centerYAnchor).isActive = true
        
        emailIconImageView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 44).isActive = true
        emailTextField.leadingAnchor.constraint(equalTo: emailIconImageView.trailingAnchor, constant: 12).isActive = true
        emailTextField.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 48).isActive = true
        let emailConstraint = emailTextField.trailingAnchor.constraint(equalTo: emailWarningView.leadingAnchor, constant: 12)
        emailConstraint.identifier = "emailConstraint"
        emailConstraint.isActive = true
        
        emailWarningView.heightAnchor.constraint(equalToConstant: 12).isActive = true
        emailWarningView.widthAnchor.constraint(equalToConstant: 12).isActive = true
        emailWarningView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -44).isActive = true
        
        emailWarningLabel.topAnchor.constraint(equalTo: emailWarningView.topAnchor).isActive = true
        emailWarningLabel.bottomAnchor.constraint(equalTo: emailWarningView.bottomAnchor).isActive = true
        emailWarningLabel.leadingAnchor.constraint(equalTo: emailWarningView.leadingAnchor).isActive = true
        emailWarningLabel.trailingAnchor.constraint(equalTo: emailWarningView.trailingAnchor).isActive = true
        
        emailLineView.heightAnchor.constraint(equalToConstant: 1.5).isActive = true
        emailLineView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 2.5).isActive = true
        emailLineView.leadingAnchor.constraint(equalTo: emailIconImageView.leadingAnchor).isActive = true
        emailLineView.trailingAnchor.constraint(equalTo: emailWarningView.trailingAnchor).isActive = true
        
        
        passwordIconImageView.centerYAnchor.constraint(equalTo: passwordTextField.centerYAnchor).isActive = true
        passwordTextField.centerYAnchor.constraint(equalTo: passwordWarningView.centerYAnchor).isActive = true
        
        passwordIconImageView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 44).isActive = true
        passwordTextField.leadingAnchor.constraint(equalTo: passwordIconImageView.trailingAnchor, constant: 12).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailLineView.bottomAnchor, constant: 28).isActive = true
        let passwordConstraint = passwordTextField.trailingAnchor.constraint(equalTo: passwordWarningView.leadingAnchor, constant: 12)
        passwordConstraint.identifier = "passwordConstraint"
        passwordConstraint.isActive = true
        
        passwordWarningView.heightAnchor.constraint(equalToConstant: 12).isActive = true
        passwordWarningView.widthAnchor.constraint(equalToConstant: 12).isActive = true
        passwordWarningView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -44).isActive = true
        
        passwordWarningLabel.topAnchor.constraint(equalTo: passwordWarningView.topAnchor).isActive = true
        passwordWarningLabel.bottomAnchor.constraint(equalTo: passwordWarningView.bottomAnchor).isActive = true
        passwordWarningLabel.leadingAnchor.constraint(equalTo: passwordWarningView.leadingAnchor).isActive = true
        passwordWarningLabel.trailingAnchor.constraint(equalTo: passwordWarningView.trailingAnchor).isActive = true
        
        passwordLineView.heightAnchor.constraint(equalToConstant: 1.5).isActive = true
        passwordLineView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 2.5).isActive = true
        passwordLineView.leadingAnchor.constraint(equalTo: passwordIconImageView.leadingAnchor).isActive = true
        passwordLineView.trailingAnchor.constraint(equalTo: passwordWarningView.trailingAnchor).isActive = true
        
        
        errorMessageLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 44).isActive = true
        errorMessageLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -44).isActive = true
        errorMessageLabel.topAnchor.constraint(equalTo: passwordLineView.bottomAnchor, constant: 14).isActive = true
        
        
        enterButton.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 44).isActive = true
        enterButton.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -44).isActive = true
        enterButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        enterButton.topAnchor.constraint(equalTo: errorMessageLabel.bottomAnchor, constant: 12).isActive = true
        
        enterButtonActivityIndicatorView.topAnchor.constraint(equalTo: enterButton.topAnchor).isActive = true
        enterButtonActivityIndicatorView.bottomAnchor.constraint(equalTo: enterButton.bottomAnchor).isActive = true
        enterButtonActivityIndicatorView.leadingAnchor.constraint(equalTo: enterButton.leadingAnchor).isActive = true
        enterButtonActivityIndicatorView.trailingAnchor.constraint(equalTo: enterButton.trailingAnchor).isActive = true
        
        layoutIfNeeded()
    }
    
    private func renderStyle() {
        backgroundColor = .appBackgroundColor
        
        logoImageView.image = .appHomeLogo
        
        titleLabel.font = UIFont.roboto(.bold, size: 24)
        titleLabel.text = "BEM VINDO AO EMPRESAS"
        titleLabel.textAlignment = .center
        titleLabel.textColor = .appCharcoalGray
        titleLabel.numberOfLines = 2
        
        subtitleLabel.font = UIFont.roboto(.regular, size: 16)
        subtitleLabel.text = "Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan."
        subtitleLabel.textAlignment = .center
        subtitleLabel.textColor = .appCharcoalGray
        subtitleLabel.numberOfLines = 2
        
        emailTextField.placeholder = "E-mail"
        emailTextField.textColor = .appCharcoalGray
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.keyboardType = .emailAddress
        emailTextField.isSecureTextEntry = false
        emailTextField.returnKeyType = .next
        
        emailWarningView.backgroundColor = .appWarningRed
        emailWarningView.layer.cornerRadius = emailWarningView.bounds.height / 2.0
        emailWarningView.layer.masksToBounds = true
        emailWarningView.isHidden = true
        
        emailWarningLabel.font = UIFont.roboto(.regular, size: 9)
        emailWarningLabel.text = "!"
        emailWarningLabel.textAlignment = .center
        emailWarningLabel.textColor = .white
        
        emailLineView.backgroundColor = .appCharcoalGray
        
        passwordTextField.placeholder = "Senha"
        passwordTextField.textColor = .appCharcoalGray
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.keyboardType = .default
        passwordTextField.isSecureTextEntry = true
        passwordTextField.returnKeyType = .go
        
        passwordWarningView.backgroundColor = .appWarningRed
        passwordWarningView.layer.cornerRadius = emailWarningView.bounds.height / 2.0
        passwordWarningView.layer.masksToBounds = true
        passwordWarningView.isHidden = true
        
        passwordWarningLabel.font = UIFont.roboto(.regular, size: 9)
        passwordWarningLabel.text = "!"
        passwordWarningLabel.textAlignment = .center
        passwordWarningLabel.textColor = .white
        
        passwordLineView.backgroundColor = .appCharcoalGray
        
        errorMessageLabel.font = UIFont.roboto(.regular, size: 10)
        errorMessageLabel.text = "Credenciais insformadas são inválidas, tente novamente."
        errorMessageLabel.textAlignment = .center
        errorMessageLabel.textColor = .appWarningRed
        errorMessageLabel.numberOfLines = 2
        errorMessageLabel.isHidden = true
        
        enterButton.setAttributedTitle(NSAttributedString(string: "ENTRAR",attributes: [
                .font: UIFont.roboto(.bold, size: 16)!,
                .foregroundColor: UIColor.white
            ]), for: .normal)
        enterButton.backgroundColor = .appInactiveGray
        enterButton.layer.cornerRadius = 4
        enterButton.layer.masksToBounds = true
        enterButton.isUserInteractionEnabled = false
        
        enterButtonActivityIndicatorView.color = .white
        enterButtonActivityIndicatorView.startAnimating()
        enterButtonActivityIndicatorView.style = .white
        enterButtonActivityIndicatorView.isHidden = true
    }
}
