//
//  LoginViewModel.swift
//  empresas
//
//  Created by Jezreel Barbosa on 26/03/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class LoginViewModel {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    
    var isEnterButtonInLoadingState = Box(false)
    var isUserInteractionEnabled = Box(true)
    var isWarningInformationHidden = Box(true)
    var isEnterButtonActive = Box(false)
    
    // Public Methods
    
    func requestLogin(email: String?, password: String?) {
        guard let email = email, let password = password, !email.isEmpty, !password.isEmpty else { return }
        
        isUserInteractionEnabled.value = false
        isEnterButtonInLoadingState.value = true
        
        AppRequest.login(with: email, password: password) { [weak self] (response, error, cache) in
            if error != nil {
                self?.authenticationDidFailed()
            }
            else if let _ = response as? [String: Any] {
                self?.authenticationDidSucceed()
            }
        }
    }
    
    // Initialisation/Lifecycle Methods
    // Override Methods
    // Private Types
    // Private Properties
    
    private let companyNavigationController = CompanyNavigationController()
    
    // Private Methods
    
    private func authenticationDidSucceed() {
        isUserInteractionEnabled.value = true
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.changeRootViewController(to: companyNavigationController)
        }
    }
    
    private func authenticationDidFailed() {
        isEnterButtonInLoadingState.value = false
        isUserInteractionEnabled.value = true
        isWarningInformationHidden.value = false
        isEnterButtonActive.value = false
    }
}
