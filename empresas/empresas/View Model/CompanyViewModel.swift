//
//  CompanyViewModel.swift
//  empresas
//
//  Created by Jezreel Barbosa on 26/03/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Foundation

class CompanyViewModel {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    
    var name: String {
        return companyDataModel.enterprise_name ?? ""
    }
    
    var typeName: String {
        return companyDataModel.enterprise_type.enterprise_type_name
    }
    
    var country: String {
        return companyDataModel.country ?? ""
    }
    
    var description: String {
        return companyDataModel.description ?? ""
    }
    
    // Public Methods
    // Initialisation/Lifecycle Methods
    
    init(_ company: Company) {
        companyDataModel = company
    }
    
    // Override Methods
    // Private Types
    // Private Properties
    
    private let companyDataModel: Company
    
    // Private Methods
}
