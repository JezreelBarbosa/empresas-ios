//
//  CompaniesViewModel.swift
//  empresas
//
//  Created by Jezreel Barbosa on 30/03/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class CompaniesViewModel {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    
    var reloadTableView = { () -> Void in }
    
    var companyViewModelList: [CompanyViewModel] = []
    
    // Public Methods
    
    func searchCompany(_ companyName: String?) {
        guard let name = companyName, !name.isEmpty else { return }
        AppRequest.search(company: name) { [weak self] (response, error, cache) in
            if let error = error {
                print(error.localizedDescription)
            } else if let response = response,
              let data = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted),
              let companyDataModelList = try? JSONDecoder().decode([Company].self, from: data, keyedBy: "enterprises") {
                
                self?.companyViewModelList.removeAll()
                companyDataModelList.forEach { (companyDataModel) in
                    self?.companyViewModelList.append(CompanyViewModel(companyDataModel))
                }
                self?.reloadTableView()
            }
            else {
                print("error decode")
            }
        }
    }
    
    func pushToCompanyDetailsViewController(from navigationController: UINavigationController?, with selectedCompanyViewModel: CompanyViewModel) {
        companyDetailsViewController.companyViewModel = selectedCompanyViewModel
        navigationController?.pushViewController(companyDetailsViewController, animated: true)
    }
    
    // Initialisation/Lifecycle Methods
    // Override Methods
    // Private Types
    // Private Properties
    
    private let companyDetailsViewController = CompanyDetailsViewController()
    
    // Private Methods
}
