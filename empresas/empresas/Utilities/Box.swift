//
//  Box.swift
//  empresas
//
//  Created by Jezreel Barbosa on 27/03/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import Foundation

final class Box<T> {
  
  typealias Listener = (T) -> Void
  
  // Static Properties
  // Static Methods
  // Public Types
  // Public Properties
  
  var listener: Listener?
  
  var value: T {
    didSet{
      listener?(value)
    }
  }
  
  // Public Methods
  
  func bind(listener: Listener?) {
    self.listener = listener
    listener?(value)
  }
  
  // Initialisation/Lifecycle Methods
  
  init(_ value: T) {
    self.value = value
  }
  
  // Override Methods
  // Private Types
  // Private Properties
  // Private Methods
}
