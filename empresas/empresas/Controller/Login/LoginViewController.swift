//
//  LoginViewController.swift
//  empresas
//
//  Created by Jezreel Barbosa on 27/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    // Public Methods
    // Initialisation/Lifecycle Methods
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        initController()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initController()
    }
    
    // Override Methods
    // Private Types
    // Private Properties
    
    private let loginView = LoginView()
    private let viewModel = LoginViewModel()
    
    // Private Methods
    
    private func initController() {
        self.view = loginView
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        loginView.emailTextField.delegate = self
        loginView.passwordTextField.delegate = self
        
        viewModel.isEnterButtonInLoadingState.bind { [weak self] state in
            self?.loginView.setEnterButtonLoadingState(state)
        }
        viewModel.isUserInteractionEnabled.bind { [weak self] state in
            self?.loginView.isUserInteractionEnabled = state
        }
        viewModel.isWarningInformationHidden.bind { [weak self] state in
            self?.loginView.setWarningInformationHidden(state)
        }
        viewModel.isEnterButtonActive.bind { [weak self] state in
            self?.loginView.setEnterButtonActive(state)
        }
        
        loginView.enterButton.addTarget(self, action: #selector(enterButtonDidTouchUpInside), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        #if DEBUG
        loginView.emailTextField.text = "testeapple@ioasys.com.br"
        loginView.passwordTextField.text = "12341234"
        viewModel.isEnterButtonActive.value = true
        #endif
    }
    
    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
    
    @objc private func enterButtonDidTouchUpInside() {
        viewModel.requestLogin(email: loginView.emailTextField.text, password: loginView.passwordTextField.text)
        hideKeyboard()
    }
}

// MARK: - Text Field Delegate

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case loginView.emailTextField:
            loginView.passwordTextField.becomeFirstResponder()
            
        case loginView.passwordTextField:
            enterButtonDidTouchUpInside()
            
        default:
            do { /* Nothing */ }
        }
        
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        viewModel.isWarningInformationHidden.value = true
        if loginView.emailTextField.hasText && loginView.passwordTextField.hasText {
            viewModel.isEnterButtonActive.value = true
        }
        else {
            viewModel.isEnterButtonActive.value = false
        }
    }
}
