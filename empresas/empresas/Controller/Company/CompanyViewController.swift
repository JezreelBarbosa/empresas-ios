//
//  CompanyViewController.swift
//  empresas
//
//  Created by Jezreel Barbosa on 28/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class CompanyViewController: UIViewController {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    // Public Methods
    // Initialisation/Lifecycle Methods
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        initController()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initController()
    }
    
    // Override Methods
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        companyView.customizeNavigationItem(self)
        companyView.customizeSearchBar()
    }
    
    // Private Types
    // Private Properties
    
    private let companyView = CompanyView()
    private let viewModel = CompaniesViewModel()
    
    private let companyDetailsViewController = CompanyDetailsViewController()
    
    // Private Methods
    
    private func initController() {
        self.view = companyView
        
        viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.companyView.tableView.reloadData()
            }
        }
        
        companyView.tableView.dataSource = self
        companyView.tableView.delegate = self
        
        companyView.tableView.register(CompanyTableViewCell.self, forCellReuseIdentifier: CompanyTableViewCell.reuseIdentifier)
        
        companyView.searchController.searchResultsUpdater = self
        navigationItem.searchController = companyView.searchController
        definesPresentationContext = true
    }
}

// MARK: - Table View Data Source
extension CompanyViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.companyViewModelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CompanyTableViewCell.reuseIdentifier) as! CompanyTableViewCell
        
        cell.fill(company: viewModel.companyViewModelList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CompanyTableViewCell.rowHeight
    }
}

// MARK: - Table View Delegate
extension CompanyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.pushToCompanyDetailsViewController(from: navigationController, with: viewModel.companyViewModelList[indexPath.row])
    }
}

// MARK: - Search Controller Update
extension CompanyViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let name = searchController.searchBar.text
        viewModel.searchCompany(name)
    }
}
