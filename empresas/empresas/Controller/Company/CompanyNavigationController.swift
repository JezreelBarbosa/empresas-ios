//
//  CompanyNavigationController.swift
//  empresas
//
//  Created by Jezreel Barbosa on 28/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class CompanyNavigationController: UINavigationController {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    // Public Methods
    // Initialisation/Lifecycle Methods
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        initController()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initController()
    }
    
    // Override Methods
    // Private Types
    // Private Properties
    
    private let companyViewController = CompanyViewController()
    
    // Private Methods
    
    private func initController() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        viewControllers = [companyViewController]
        
        navigationBar.isTranslucent = false
        navigationBar.tintColor = .white
        navigationBar.backgroundColor = .clear
        navigationBar.barTintColor = .appMediumPink
        view.backgroundColor = .appMediumPink
    }
}
