//
//  CompanyDetailsViewController.swift
//  empresas
//
//  Created by Jezreel Barbosa on 28/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class CompanyDetailsViewController: UIViewController {
    // Static Properties
    // Static Methods
    // Public Types
    // Public Properties
    // Public Methods
    
    weak var companyViewModel: CompanyViewModel?
    
    // Initialisation/Lifecycle Methods
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        initController()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initController()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // Override Methods
    
    override func viewWillAppear(_ animated: Bool) {
        detailsView.fillCompanyDetails(self, company: companyViewModel)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        detailsView.clearInformation()
    }
    
    // Private Types
    // Private Properties
    
    private let detailsView = CompanyDetailsView()
    
    // Private Methods
    
    private func initController() {
        self.view = detailsView
    }
}
