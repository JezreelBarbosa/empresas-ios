//
//  AppRequest.swift
//  empresas
//
//  Created by Jezreel Barbosa on 28/02/20.
//  Copyright © 2020 ioasys. All rights reserved.
//

import UIKit

class AppRequest: APIRequest {
    // Static Properties
    // Static Methods
    
    @discardableResult
    static func login(with username: String, password: String, callback: ResponseBlock<Any>?) -> AppRequest {
        let request = AppRequest(method: .post, path: "users/auth/sign_in", parameters: ["email": username, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            callback?(response, error, cache)
        }
        request.shouldSaveInCache = false
        
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func search(company companyName: String, callback: ResponseBlock<Any>?) -> AppRequest {
        let request = AppRequest(method: .get, path: "enterprises", parameters: nil, urlParameters: ["name": companyName], cacheOption: .networkOnly) { (response, error, cache) in
            callback?(response, error, cache)
        }
        request.shouldSaveInCache = false
        request.task?.cancel()
        request.makeRequest()
        return request
    }
    
    // Public Types
    // Public Properties
    // Public Methods
    // Initialisation/Lifecycle Methods
    
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "https://empresas.ioasys.com.br/api/v1/")!
    }
    
    // Override Methods
    // Private Types
    // Private Properties
    // Private Methods
}

// MARK: - Help
extension JSONDecoder {
    func decode<T: Decodable>(_ type: T.Type, from data: Data, keyedBy key: String?) throws -> T {
        if let key = key {
            // Pass the top level key to the decoder
            userInfo[.jsonDecoderRootKeyName] = key
            let root = try decode(DecodableRoot<T>.self, from: data)
            return root.value
        } else {
            return try decode(type, from: data)
        }
    }
    
}

extension CodingUserInfoKey {
    static let jsonDecoderRootKeyName = CodingUserInfoKey(rawValue: "rootKeyName")!
}

struct DecodableRoot<T>: Decodable where T: Decodable {
    
    private struct CodingKeys: CodingKey {
        var stringValue: String
        var intValue: Int?
        
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        
        init?(intValue: Int) {
            self.intValue = intValue
            stringValue = "\(intValue)"
        }
        
        static func key(named name: String) -> CodingKeys? {
            return CodingKeys(stringValue: name)
        }
        
    }
    
    let value: T
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        guard
          let keyName = decoder.userInfo[.jsonDecoderRootKeyName] as? String,
          let key = CodingKeys.key(named: keyName) else {
            throw DecodingError.valueNotFound(T.self, DecodingError.Context.init(codingPath: [], debugDescription: "Value not found at root level"))
        }
 
        value = try container.decode(T.self, forKey: key)
    }
    
}
